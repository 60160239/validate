const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=>{
    context('function isUserNameValid', ()=>{
        it(' 1.1) Function prototype : boolean isUserNameValid(username: String)', ()=>{
            expect(validate.isUserNameValid('kob')).to.be.true;
        })
        it(' 1.2) จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=>{
            expect(validate.isUserNameMoreThanThree('tu')).to.be.false;
        })
        
        it(' 1.3) ทุกตัวต้องเป็นตัวเล็ก', ()=>{
            expect(validate.isUserNameLowerCase('Kob')).to.be.false;
            expect(validate.isUserNameLowerCase('koB')).to.be.false;
        })
        it(' 1.4) จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=>{
            expect(validate.isUserNameMoreThanFifteen('kob123456789012')).to.be.false;
            expect(validate.isUserNameMoreThanFifteen('kob1234567890123')).to.be.true;
        })
        it(' 2.1) Function prototype : boolean isAgeValid (age: String)', ()=>{
            expect(validate.isAgeValid(18)).to.be.true;
        })
        it(' 2.2) age ต้องเป็นข้อความที่เป็นตัวเลข', ()=>{
            expect(validate.isAgeNumber('what')).to.be.false;
            expect(validate.isAgeNumber(10)).to.be.true;
        })
        it(' 2.3) อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=>{
            expect(validate.isAgeNotGood(17)).to.be.false;
            expect(validate.isAgeNotGood(18)).to.be.true;
            expect(validate.isAgeNotGood(100)).to.be.true;
            expect(validate.isAgeNotGood(101)).to.be.false;
        })
        it(' 3.1) Function prototype : boolean isUserNameValid(password: String)', ()=>{
            expect(validate.isPasswordValid('password')).to.be.true;
        })
        it(' 3.2) จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=>{
            expect(validate.isPasswordNotLessEight('passwor')).to.be.false;
            expect(validate.isPasswordNotLessEight('password')).to.be.true;
        })
        it(' 3.3) ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=>{
            
        })
        it(' 3.4) ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=>{
            
        })
        it(' 3.5) ต้องมีอักขระ พิเศษ อย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordHasSpecialCharacter('mypassword')).to.be.false;
            expect(validate.isPasswordHasSpecialCharacter('@mypassword')).to.be.true;
        })
        it(' 4.1) Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', ()=>{
            
        })
        it(' 4.2) day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', ()=>{
            
        })
        it(' 4.3) month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', ()=>{
            
        })
        it(' 4.4) year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', ()=>{
            
        })
        it(' 4.5) เดือนแต่ละเดือนมีจำนวนวันต่างกันตามรายการ', ()=>{
            
        })
        it(' 4.6) ในกรณีของปี อธิกสุรทิน ให้คำนวนตามหลักเกณฑ์', ()=>{
            
        })
    })
})